package com.java.sms.listener;

import com.aliyuncs.exceptions.ClientException;
import com.java.sms.utils.SmsUtil;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Map;

/**
 * @author jl
 * @description
 * @date 2019-11-05 9:10
 */
@Component
public class SmsListener {
    @Autowired
    private SmsUtil smsUtil;

    @Value("${aliyun.sms.signName}")
    private String signName;

    @Value("${aliyun.sms.templateCode}")
    private String templateCode;

    @RabbitListener(bindings = @QueueBinding(
            //声明队列
            value = @Queue(value = "LEYOU.USER.SMS.QUEUE",durable = "true"),
            //声明交换机
            exchange = @Exchange(value = "LEYOU.USER.EXCHANGE",ignoreDeclarationExceptions = "true",type = ExchangeTypes.TOPIC),
            //routingKey
            key = {"user.registry"}
    ))
    public void sendSms(Map<String, String> map) {
        if (CollectionUtils.isEmpty(map)) {
            return;
        }
        String mobile = map.get("phone");
        String smsCode = map.get("smsCode");
        try {
            //为了扩展,发送其它模板的验证码
            smsUtil.sendSms(mobile, templateCode, signName, smsCode);
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }
}
