package com.java.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.order.dao.OrderStatusDao;
import com.java.order.entity.OrderStatus;
import com.java.order.service.OrderStatusService;
import org.springframework.stereotype.Service;


@Service("orderStatusService")
public class OrderStatusServiceImpl extends ServiceImpl<OrderStatusDao, OrderStatus> implements OrderStatusService {


}