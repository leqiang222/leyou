package com.java.item.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.item.entity.Category;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 商品类目表，类目和商品(spu)是一对多关系，类目与品牌是多对多关系
 * 
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-10-11 11:44:02
 */
@Mapper
public interface CategoryDao extends BaseMapper<Category> {

    @Select("SELECT * FROM `tb_category` a LEFT JOIN `tb_category_brand` b ON a.`id`=b.`category_id` WHERE b.`brand_id` = #{bid}")
    List<Category> queryCategoryByBid(Long bid);
}
