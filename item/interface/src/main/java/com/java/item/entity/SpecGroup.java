package com.java.item.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * 规格参数的分组表，每个商品分类下有多个规格参数组
 * 
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-10-16 11:30:52
 */
@Data
@TableName("tb_spec_group")
public class SpecGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long id;
	/**
	 * 商品分类id，一个分类下有多个规格组
	 */
	private Long cid;
	/**
	 * 规格组的名称
	 */
	private String name;
	/**
	 * 参数值
	 */
	@TableField(exist = false)
	private List<SpecParam> params;

}
