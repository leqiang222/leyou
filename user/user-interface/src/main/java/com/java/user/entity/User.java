package com.java.user.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

/**
 * 用户表
 * 
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-11-04 16:45:42
 */
@Data
@TableName("tb_user")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 用户名
	 */
	private String username;
	/**
	 * 密码，加密存储
	 */
	@JsonIgnore //对象序列化为json字符串时忽略该属性
	private String password;
	/**
	 * 注册手机号
	 */
	private String phone;
	/**
	 * 创建时间
	 */
	private Date created;
	/**
	 * 密码加密的salt值
	 */
	@JsonIgnore
	private String salt;

}
