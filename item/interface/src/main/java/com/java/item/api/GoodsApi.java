package com.java.item.api;

import com.java.common.dato.CartDTO;
import com.java.common.utils.PageResult;
import com.java.item.dto.SpuDTO;
import com.java.item.entity.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author jl
 * @description
 * @date 2019-10-25 16:22
 */
public interface GoodsApi {
    /**
     * 根据spuId查询商品详情spuDetail
     */
    @GetMapping("/spu/detail/{spuId}")
    SpuDetail queryDetailBySpuId(@PathVariable("spuId") Long spuId);

    /**
     * 根据条件分页查询spu
     */
    @GetMapping("/spu/page")
    PageResult<SpuDTO> querySpuByPage(@RequestParam(value = "key",required = false) String key,
                                      @RequestParam(value = "saleable",required = false) Boolean saleable,
                                      @RequestParam(value = "page",defaultValue = "1") Integer page,
                                      @RequestParam(value = "rows",defaultValue = "5") Integer rows);

    /**
     * 根据spuId查询sku集合
     */
    @GetMapping("/sku/list")
    List<Sku> querySkusBySpuId(@RequestParam("id")Long spuId);

    /**
     * 根据brandId查询品牌
     */
    @GetMapping("/brand/{id}")
    Brand queryBrandById(@PathVariable("id") Long id);

    /**
     * 根据分类ids查询分类名称集合
     */
    @GetMapping("/category")
    List<String> queryCategoryNamesByIds(@RequestParam("ids") List<Long> ids);

    /**
     * 根据传入的条件查询规格参数
     */
    @GetMapping("/spec/params")
    List<SpecParam> queryParams(@RequestParam(value = "gid",required = false) Long gid,
                                                       @RequestParam(value = "cid",required = false) Long cid,
                                                       @RequestParam(value = "generic",required = false) Boolean generic,
                                                       @RequestParam(value = "searching",required = false) Boolean searching);

    /**
     * 根据spuId查询Spu
     */
    @GetMapping("{id}")
    Spu querySpuById(@PathVariable("id") Long id);

    /**
     * 根据传cid查询规格参数
     */
    @GetMapping("/spec/groups/{cid}")
    List<SpecGroup> queryGroupsByCid(@PathVariable("cid") Long cid);

    /**
     * 根据skuId查询sku
     */
    @GetMapping("/sku/{skuId}")
    Sku querySkuBySkuId(@PathVariable("skuId") Long skuId);

    /**
     * 减库存接口
     */
    @PostMapping("stock/decrease")
    Void decreaseStock(@RequestBody List<CartDTO> carts);
}
