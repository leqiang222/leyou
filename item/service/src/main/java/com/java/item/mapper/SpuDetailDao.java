package com.java.item.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.item.entity.SpuDetail;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-10-16 11:30:52
 */
@Mapper
public interface SpuDetailDao extends BaseMapper<SpuDetail> {
	
}
