package com.java.item.controller;

import com.java.item.entity.SpecGroup;
import com.java.item.entity.SpecParam;
import com.java.item.service.SpecificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author jl
 * @description
 * @date 2019-10-16 14:21
 */
@Controller
@RequestMapping("/spec")
public class SpecificationController {
    @Autowired
    private SpecificationService specificationService;

    /**
     * 根据传cid查询规格参数
     */
    @GetMapping("/groups/{cid}")
    public ResponseEntity<List<SpecGroup>> queryGroupsByCid(@PathVariable("cid") Long cid) {
        List<SpecGroup> specGroups = specificationService.queryGroupsByCid(cid);
        return ResponseEntity.ok(specGroups);
    }

    /**
     * 根据传入的条件查询规格参数
     */
    @GetMapping("/params")
    public ResponseEntity<List<SpecParam>> queryParams(@RequestParam(value = "gid",required = false) Long gid,
                                                       @RequestParam(value = "cid",required = false) Long cid,
                                                       @RequestParam(value = "generic",required = false) Boolean generic,
                                                       @RequestParam(value = "searching",required = false) Boolean searching
                                                       ) {
        List<SpecParam> specParams = specificationService.queryParams(gid,cid,generic,searching);
        return ResponseEntity.ok(specParams);
    }
}
