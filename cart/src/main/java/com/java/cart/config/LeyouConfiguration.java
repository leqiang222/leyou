package com.java.cart.config;

import com.java.cart.interceptor.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author jl
 * @description
 * @date 2019-11-07 15:02
 */
@Configuration
public class LeyouConfiguration implements WebMvcConfigurer {
    @Autowired
    private LoginInterceptor loginInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //  /** 所有路径    /* 一级路径
        registry.addInterceptor(loginInterceptor).addPathPatterns("/**");
    }
}
