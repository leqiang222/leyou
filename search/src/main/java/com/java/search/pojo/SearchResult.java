package com.java.search.pojo;

import com.java.common.utils.PageResult;
import com.java.item.entity.Brand;

import java.util.List;
import java.util.Map;

public class SearchResult extends PageResult<Goods> {
    private List<Map<String,Object>> categories;  //根据分类id聚合的结果
    private List<Brand> brands;  //根据品牌id聚合的结果
    private List<Map<String,Object>> specs;  //根据规格参数聚合的结果

    public SearchResult(Long total, Long totalPage, List<Goods> items, List<Map<String,Object>> categories, List<Brand> brands, List<Map<String, Object>> specs) {
        super(total, totalPage, items);
        this.categories = categories;
        this.brands = brands;
        this.specs = specs;
    }

    public List<Map<String,Object>> getCategories() {
        return categories;
    }

    public void setCategories(List<Map<String,Object>> categories) {
        this.categories = categories;
    }

    public List<Brand> getBrands() {
        return brands;
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

    public List<Map<String, Object>> getSpecs() {
        return specs;
    }

    public void setSpecs(List<Map<String, Object>> specs) {
        this.specs = specs;
    }
}