package com.java.goodsweb.service.impl;

import com.java.goodsweb.feign.GoodsApiFeign;
import com.java.goodsweb.service.GoodsService;
import com.java.item.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author jl
 * @description
 * @date 2019-10-31 14:11
 */
@Service
public class GoodsServiceImpl implements GoodsService {
    @Autowired
    private GoodsApiFeign goodsApiFeign;

    @Override
    public Map<String, Object> loadData(Long spuId) {
        Map<String,Object> model = new HashMap<>();

        //根据spuId查询spu
        Spu spu = goodsApiFeign.querySpuById(spuId);
        //查询spuDetail
        SpuDetail spuDetail = goodsApiFeign.queryDetailBySpuId(spuId);

        //查询分类: Map<String,Object>
        List<Long> cids = Arrays.asList(spu.getCid1(), spu.getCid2(), spu.getCid3());
        List<String> names = goodsApiFeign.queryCategoryNamesByIds(cids);
        //初始化一个分类的集合
        List<Map<String,Object>> categories = new ArrayList<>();
        for (int i = 0; i < cids.size(); i++) {
            Map<String,Object> map = new HashMap<>();
            map.put("id",cids.get(i));
            map.put("name",names.get(i));
            categories.add(map);
        }

        //查询品牌
        Brand brand = goodsApiFeign.queryBrandById(spu.getBrandId());
        //skus
        List<Sku> skus = goodsApiFeign.querySkusBySpuId(spuId);
        //查询规格参数组
        List<SpecGroup> groups = goodsApiFeign.queryGroupsByCid(spu.getCid3());
        //查询特殊的规格参数
        List<SpecParam> params = goodsApiFeign.queryParams(null, spu.getCid3(), false, null);
        //初始化特殊规格参数的map
        Map<Long,String> paramMap = new HashMap<>();
        params.forEach(param -> paramMap.put(param.getId(),param.getName()));

        model.put("spu",spu);
        model.put("spuDetail",spuDetail);
        model.put("categories",categories);
        model.put("brand",brand);
        model.put("skus",skus);
        model.put("groups",groups);
        model.put("paramMap",paramMap);

        return model;
    }
}
