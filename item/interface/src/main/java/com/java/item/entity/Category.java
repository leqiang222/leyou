package com.java.item.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import lombok.Data;

/**
 * 商品类目表，类目和商品(spu)是一对多关系，类目与品牌是多对多关系
 * 
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-10-11 11:44:02
 */
@Data
@TableName("tb_category")
public class Category implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 类目id
	 */
	@TableId
	private Long id;
	/**
	 * 类目名称
	 */
	private String name;
	/**
	 * 父类目id,顶级类目填0
	 */
	private Long parentId;
	/**
	 * 是否为父节点，0为否，1为是
	 */
	private Boolean isParent;
	/**
	 * 排序指数，越小越靠前
	 */
	private Integer sort;

}
