package com.java.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.auth.entity.UserInfo;
import com.java.common.dato.CartDTO;
import com.java.common.exception.CusException;
import com.java.common.utils.PageResult;
import com.java.order.dao.OrderDao;
import com.java.order.dao.OrderStatusDao;
import com.java.order.entity.Order;
import com.java.order.entity.OrderDetail;
import com.java.order.entity.OrderStatus;
import com.java.order.feign.GoodsApiFeign;
import com.java.order.interceptor.LoginInterceptor;
import com.java.order.service.OrderDetailService;
import com.java.order.service.OrderService;
import com.java.order.service.OrderStatusService;
import com.java.order.utils.PayHelper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("orderService")
public class OrderServiceImpl extends ServiceImpl<OrderDao, Order> implements OrderService {
    @Autowired
    private OrderDetailService orderDetailService;
    @Autowired
    private OrderStatusService orderStatusService;
    @Autowired
    private GoodsApiFeign goodsApiFeign;
    @Autowired
    private PayHelper payHelper;

    private static final Logger logger = LoggerFactory.getLogger(OrderService.class);

    /**
     * 不在此判断库存（线程不安全会造成超卖），在sql中判断库存：若库存不足sql抛出异常使事务回滚
     */
    @Transactional
    public Long createOrder(Order order) {
        // 生成orderId
        long orderId = IdWorker.getId();
        // 获取登录用户
        UserInfo user = LoginInterceptor.getUserInfo();
        // 初始化数据
        order.setBuyerNick(user.getUsername());
        order.setBuyerRate(false);
        order.setCreateTime(new Date());
        order.setOrderId(orderId);
        order.setUserId(String.valueOf(user.getId()));
        // 保存数据
        this.baseMapper.insert(order);

        // 保存订单状态
        OrderStatus orderStatus = new OrderStatus();
        orderStatus.setOrderId(orderId);
        orderStatus.setCreateTime(order.getCreateTime());
        orderStatus.setStatus(1);// 初始状态为未付款

        orderStatusService.save(orderStatus);

        // 订单详情中添加orderId
        order.getOrderDetail().forEach(od -> od.setOrderId(orderId));
        // 保存订单详情,使用批量插入功能
        orderDetailService.saveBatch(order.getOrderDetail());

        //对订单中每一个商品减库存
        List<CartDTO> cartDTOS = new ArrayList<>();
        CartDTO cartDTO = new CartDTO();
        order.getOrderDetail().forEach(cart -> {
                    cartDTO.setSkuId(cart.getSkuId());
                    cartDTO.setNum(cart.getNum());
                    cartDTOS.add(cartDTO);
                }
        );
        goodsApiFeign.decreaseStock(cartDTOS);

        logger.debug("生成订单，订单编号：{}，用户id：{}", orderId, user.getId());
        return orderId;
    }

    private Order queryById(Long id) {
        // 查询订单
        Order order = this.baseMapper.selectById(id);

        // 查询订单详情
        List<OrderDetail> details = orderDetailService.list(new LambdaQueryWrapper<OrderDetail>().eq(OrderDetail::getOrderId,id));
        order.setOrderDetail(details);

        // 查询订单状态
        OrderStatus status = orderStatusService.getById(order.getOrderId());
        order.setStatus(status.getStatus());
        return order;
    }

    public PageResult<Order> queryUserOrderList(Integer page, Integer rows, Integer status) {
        try {
            // 获取登录用户
            UserInfo user = LoginInterceptor.getUserInfo();
            // 创建查询条件
            IPage<Order> orders = this.baseMapper.selectPage(new Page<>(page, rows), new LambdaQueryWrapper<Order>().eq(Order::getUserId, user.getId()).eq(Order::getStatus,status));
            return new PageResult<>(orders.getTotal(), orders.getPages(),orders.getRecords());
        } catch (Exception e) {
            logger.error("查询订单出错", e);
            return null;
        }
    }

    @Transactional
    public Boolean updateStatus(Long id, Integer status) {
        OrderStatus record = new OrderStatus();
        record.setOrderId(id);
        record.setStatus(status);
        // 根据状态判断要修改的时间
        switch (status) {
            case 2:
                record.setPaymentTime(new Date());// 付款
                break;
            case 3:
                record.setConsignTime(new Date());// 发货
                break;
            case 4:
                record.setEndTime(new Date());// 确认收获，订单结束
                break;
            case 5:
                record.setCloseTime(new Date());// 交易失败，订单关闭
                break;
            case 6:
                record.setCommentTime(new Date());// 评价时间
                break;
            default:
                return null;
        }
        return orderStatusService.updateById(record);
    }


    public String createPayUrl(Long orderId) {
        //1、查询订单
        Order order = queryById(orderId);
        //2、判断订单状态
        Integer status = order.getStatus();
        if (status != 1) {  //若非未付款状态抛出异常
            //抛异常
            throw new CusException("订单状态异常!");
        }
        //计算总支付金额
        Long actualPay = order.getActualPay();
        //商品描述
        String desc = "乐优商城支付测试";
        //向微信发起下单请求，取得返回的二维码付款地址
        return payHelper.createPayUrl(orderId, actualPay, desc);
    }

    /**
     * 完成对xml数据的校验
     *
     * @param result 支付成功后微信返回xml数据序列化后的map
     */
    public void handleNotify(Map<String, String> result) {
        String outTradeNoStr = result.get("out_trade_no");
        String totalFeeStr = result.get("total_fee");
        if (StringUtils.isBlank(outTradeNoStr) || StringUtils.isBlank(totalFeeStr)) {
            //TODO 抛出解析数据失败的异常
        }
        Long orderId = Long.parseLong(outTradeNoStr); //订单编号
        Long totalFee = Long.parseLong(totalFeeStr); //应付金额

        //1、根据通信、业务标识判断微信下单是否成功,若不成功则抛异常
        payHelper.isSuccess(orderId, result);
        //2、校验签名,若不成功则抛异常
        payHelper.validSign(result);

        //3、校验应付金额
        Order order = this.baseMapper.selectById(orderId);
        if (totalFee != order.getActualPay()) {  //应付金额不符
            //TODO 抛出应付金额不符异常
        }

        //4、修改订单状态为已付款
        OrderStatus os = new OrderStatus();
        os.setStatus(2);
        os.setOrderId(orderId);
        os.setPaymentTime(new Date());
        boolean effectedRow = orderStatusService.updateById(os);
        //修改订单状态失败
        if (!effectedRow) {
            //TODO 抛出修改订单状态失败异常
        }
    }
}