package com.java.item.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java.common.utils.PageResult;
import com.java.common.utils.QueryParams;
import com.java.item.entity.Brand;
import com.java.item.entity.CategoryBrand;
import com.java.item.mapper.BrandDao;
import com.java.item.service.BrandService;
import com.java.item.service.CategoryBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


@Service("brandService")
public class BrandServiceImpl extends ServiceImpl<BrandDao, Brand> implements BrandService {
    @Autowired
    private BrandDao brandDao;
    @Autowired
    private CategoryBrandService categoryBrandService;

    @Override
    public PageResult<Brand> queryBrandsByPage(QueryParams queryParams) {
        //根据name模糊查询,或者根据首字母查询
        QueryWrapper<Brand> wrapper = new QueryWrapper<>();
        if (StrUtil.isNotEmpty(queryParams.getKey())) {
            wrapper.like("name",queryParams.getKey()).or().eq("letter",queryParams.getKey());
        }
        //排序
        if (StrUtil.isNotEmpty(queryParams.getSortBy())){
            wrapper.orderBy(true,queryParams.getDesc(),queryParams.getSortBy());
        }
        //分页查询
        IPage<Brand> page = brandDao.selectPage(new Page<>(queryParams.getPage(), queryParams.getRows()), wrapper);
        return new PageResult<>(page.getTotal(),page.getPages(),page.getRecords());
    }

    @Override
    @Transactional
    public void saveBrand(Brand brand, List<Long> cids) {
        //新增品牌
        this.save(brand);
        //新增分类和品牌关系表
        List<CategoryBrand> collect = cids.stream().map(cid -> new CategoryBrand(cid, brand.getId())).collect(Collectors.toList());
        categoryBrandService.saveBatch(collect);
    }

    @Override
    public List<Brand> queryByCid(Long cid) {
        return brandDao.queryByCid(cid);
    }

    @Override
    @Transactional
    public void updateBrand(Brand brand, List<Long> cids) {
        this.updateById(brand);
        //修改分类和品牌关系表
        //1.删除旧的
        categoryBrandService.remove(new LambdaQueryWrapper<CategoryBrand>().eq(CategoryBrand::getBrandId,brand.getId()));
        //2.新增分类和品牌关系表
        List<CategoryBrand> collect = cids.stream().map(cid -> new CategoryBrand(cid, brand.getId())).collect(Collectors.toList());
        categoryBrandService.saveBatch(collect);
    }

    @Override
    @Transactional
    public void deleteBrand(Long bid) {
        //删除品牌表
        this.removeById(bid);
        //删除分类和品牌关系表
        categoryBrandService.remove(new LambdaQueryWrapper<CategoryBrand>().eq(CategoryBrand::getBrandId,bid));
    }

}