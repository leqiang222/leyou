package com.java.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.common.utils.PageResult;
import com.java.order.entity.Order;

import java.util.Map;


/**
 * 
 *
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-11-08 14:20:23
 */
public interface OrderService extends IService<Order> {

    Long createOrder(Order order);

    PageResult<Order> queryUserOrderList(Integer page, Integer rows, Integer status);

    Boolean updateStatus(Long id, Integer status);

    String createPayUrl(Long orderId);

    void handleNotify(Map<String, String> result);
}

