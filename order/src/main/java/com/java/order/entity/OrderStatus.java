package com.java.order.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 订单状态表
 * 
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-11-08 14:20:23
 */
@Data
@TableName("tb_order_status")
public class OrderStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 订单id
	 */
	@TableId(type = IdType.INPUT)
	private Long orderId;
	/**
	 * 状态：1、未付款 2、已付款,未发货 3、已发货,未确认 4、交易成功 5、交易关闭 6、已评价
	 */
	private Integer status;
	/**
	 * 订单创建时间
	 */
	private Date createTime;
	/**
	 * 付款时间
	 */
	private Date paymentTime;
	/**
	 * 发货时间
	 */
	private Date consignTime;
	/**
	 * 交易完成时间
	 */
	private Date endTime;
	/**
	 * 交易关闭时间
	 */
	private Date closeTime;
	/**
	 * 评价时间
	 */
	private Date commentTime;

}
