package com.java.item.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author jiangli
 * @email 31346337@qq.com
 * @date 2019-10-16 11:30:52
 */
@Data
@TableName("tb_spu_detail")
public class SpuDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(type = IdType.INPUT)
	private Long spuId;
	/**
	 * 商品描述信息
	 */
	private String description;
	/**
	 * 通用规格参数数据
	 */
	private String genericSpec;
	/**
	 * 特有规格参数及可选值信息，json格式
	 */
	private String specialSpec;
	/**
	 * 包装清单
	 */
	private String packingList;
	/**
	 * 售后服务
	 */
	private String afterService;

}
