package com.java.goodsweb.feign;

import com.java.item.api.GoodsApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author jl
 * @description
 * @date 2019-10-25 16:15
 */
@FeignClient("item-service")
public interface GoodsApiFeign extends GoodsApi {

}
